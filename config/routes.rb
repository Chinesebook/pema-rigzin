Rails.application.routes.draw do
  # get 'user_session/new'
  # get 'user_session/create'
  # get 'user_session/destroy'

  # resources :users

  constraints subdomain: 'admin' do
    scope as: :admin, module: :admin do
      resources :articles
      root 'articles#index'
    end
    resources :users

    controller :user_session do
      get    'login'  => :new
      post   'login'  => :create
      delete 'logout' => :destroy
    end
    # get '/', to: 'users#index', constraints: { subdomain: 'admin' }

    namespace :upload do
      resources :images, only: [:create, :destroy]
    end
  end

  constraints subdomain: 'api' do
    scope as: :api, module: :api do
      resources :articles, only: [:index, :show]
    end
  end

  resources :articles, only: [:index, :show]


  get '/', to: 'articles#index', constraints: { subdomain: 'www' }
  root 'articles#index', as: 'articles_index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
