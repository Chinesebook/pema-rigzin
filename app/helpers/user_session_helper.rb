module UserSessionHelper
  def user_logged_in?
    if session[:user_id]
      true
    else
      false
    end
  end

  def admin?
    user = User.find(session[:user_id])
    user.admin
  end
end
