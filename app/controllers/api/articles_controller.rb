class API::ArticlesController < ApplicationController
  skip_before_action :authorize, only: [:index, :show]

  def index
    @articles = Article.order(id: :desc)
    render json: @articles.as_json(include: { user: { only: :name } })
  end
end
