class ArticlesController < WebController
  skip_before_action :authorize, only: [:index, :show]

  # GET /articles
  # GET /articles.json
  def index
    if params[:category].blank?
      # n + 1 queries https://ihower.tw/rails/performance.html
      @articles = Article.includes(:user).order(id: :desc)
    else
      @articles = Article.includes(:user).where(category: params[:category]).order(id: :desc)
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @article = Article.find(params[:id])
  end
end
