class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authorize
  # around_action :with_locale
  around_action :with_time_zone


  protected
    def authorize
      unless User.find_by(id: session[:user_id])
        redirect_to login_url, notice: '請先登入'
      end
    end

    # def with_locale
    #   I18n.with_locale('zh-TW') { yield }
    # end

    def with_time_zone
      Time.use_zone('Asia/Taipei') { yield }
    end
end
