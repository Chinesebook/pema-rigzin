class Upload::ImagesController < ApplicationController
  skip_before_action :authorize

  def create
    @image = Upload::Image.new(image_params)
    respond_to do |format|
      if @image.save
        format.html
        format.json { render :show, status: :created }
      else
        format.html { render :new }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @image = Upload::Image.find(params[:id])
    @image.destroy
    respond_to do |format|
      format.html
      format.json { head :no_content }
    end
  end

  private
    def image_params
      params.require(:image).permit(:image)
    end
end
