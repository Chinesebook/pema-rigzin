class UserSessionController < AdminController
  skip_before_action :authorize

  def new
  end

  def create
    user = User.find_by(account: params[:account])
    if user.try(:authenticate, params[:password])
      session[:user_id] = user.id
      redirect_to articles_index_url
    else
      redirect_to login_url, alert: '帳號密碼錯誤'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to articles_index_url, notice: '已經登出'
  end
end
