//= require quill.core
//= require quill
//= require quill.min

document.addEventListener('turbolinks:load', function() {
  if (document.querySelector('#editor-container')) {
    function imageHandler() {
      // console.log("in imageHandler" + this);
      var Delta = Quill.imports.delta
      // let fileInput = this.container.querySelector('input.ql-image[type=file]');
      var fileInput = this.container.querySelector('input.ql-image[type=file]');
      // console.log("after var" + this);
      // 第一次點擊的時候querySelector找不到東西，所以建立
      if (fileInput == null) {
        fileInput = document.createElement('input');
        fileInput.setAttribute('type', 'file');
        fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon');
        fileInput.classList.add('ql-image');
        // 選擇上傳檔案後值會改變
        // fileInput.addEventListener('change', () => {
        fileInput.addEventListener('change', function () {
          // console.log("in event listener" + this);
          if (fileInput.files != null && fileInput.files[0] != null) {
            // console.log("in if" + this);
            // ***** 以下為原始程式碼 *****
            // // https://developer.mozilla.org/zh-TW/docs/Web/API/FileReader
            // let reader = new FileReader();
            // reader.onload = (e) => {
            //   // 檢查使用者游標選取的範圍
            //   // https://quilljs.com/docs/api/#getselection
            //   let range = this.quill.getSelection(true);
            //   // 以圖片取代範圍內的內容
            //   // https://quilljs.com/docs/api/#updatecontents
            //   this.quill.updateContents(new Delta()
            //     .retain(range.index)
            //     .delete(range.length)
            //     .insert({ image: e.target.result })
            //   // Emitter.sources.USER);
            //   , Quill.sources.USER);
            //   // this.quill.setSelection(range.index + 1, Emitter.sources.SILENT);
            //   this.quill.setSelection(range.index + 1, Quill.sources.SILENT);
            //   fileInput.value = "";
            // }
            // // Convert image to base64
            // // https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
            // reader.readAsDataURL(fileInput.files[0]);

            // ***** 魔改開始 *****
            const fd = new FormData();
            // rails params
            fd.append('image[image]', fileInput.files[0]);

            const xhr = new XMLHttpRequest();
            xhr.open('POST', window.location.origin + '/upload/images');
            // 希望伺服器回傳json
            xhr.setRequestHeader("Accept", "application/json");
            Rails.CSRFProtection(xhr);
            // xhr.onload = () => {
            xhr.onload = function () {
              // status: :created status為201
              if (xhr.status === 201) {
                // this is callback data: url
                // console.log("in xhr" + this);
                const url = JSON.parse(xhr.responseText).url;
                const range = this.quill.getSelection();
                this.quill.insertEmbed(range.index, 'image', url);
                this.quill.setSelection(range.index + 1, Quill.sources.SILENT);
              }
            }.bind(this);
            xhr.send(fd);
            // 魔改結束
          }
        }.bind(this));
        this.container.appendChild(fileInput);
      }
      fileInput.click();
    }

    var toolbarOptions = [
      [
        { 'header': [1, 2, 3, false] },
        { 'font': [] },
        { 'size': ['small', false, 'large'] }
      ],

      [
        'bold', 'italic', 'underline', 'strike',
        { 'color': [] }, { 'background': [] }
      ],

      ['link', 'image'],

      [ { 'direction': 'rtl' }, { 'align': [] } ],
      [
        { 'list': 'ordered' }, { 'list': 'bullet' },
        { 'indent': '-1' }, { 'indent': '+1' }
      ],

      ['clean']
    ];

    var quill = new Quill('#editor-container', {
      modules: {
        toolbar: toolbarOptions
      },
      theme: 'snow',
    });

    var toolbar = quill.getModule('toolbar');
    toolbar.addHandler('image', imageHandler);

    // 當submit的時候將editor的內容塞到article_content
    var form = document.querySelector('form');
    form.addEventListener('submit', function () {
      var content = document.querySelector('#article_content');
      content.value = document.querySelector('.ql-editor').innerHTML;
    });
  }
});
