# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# # 頁面讀取完後執行
# # 如果#editor存在就替換quill
# document.addEventListener('turbolinks:load', () ->
#   if document.querySelector('#editor-container')
#     # 修改上傳圖片功能 https://github.com/quilljs/quill/issues/1089

#     # 格式可以在這邊找到 https://quilljs.com/docs/formats/
#     toolbarOptions = [
#       [
#         { 'header': [1, 2, 3, false] },
#         { 'font': [] },
#         { 'size': ['small', false, 'large'] }
#       ],

#       [
#         'bold', 'italic', 'underline', 'strike',
#         { 'color': [] }, { 'background': [] }
#       ],

#       ['link', 'image'],

#       [{ 'direction': 'rtl' }, { 'align': [] }],

#       [
#         { 'list': 'ordered'}, { 'list': 'bullet' },
#         { 'indent': '-1'}, { 'indent': '+1' }
#       ],

#       ['clean']
#     ]

#     quill = new Quill('#editor-container', {
#       modules: {
#         toolbar: toolbarOptions
#       },
#       theme: 'snow',
#       # imageHandler: imageHandler
#     })
#     form = document.querySelector('form')
#     form.addEventListener('submit', (e) ->
#       content = document.querySelector('#article_content')
#       content.value = document.querySelector('.ql-editor').innerHTML
#       # console.log(content.value)
#       # Event.preventDefault(): 防止原本的事件發生 ex:換頁
#       # e.preventDefault()
#     )
# )
