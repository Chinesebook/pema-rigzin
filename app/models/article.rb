class Article < ApplicationRecord

  enum category: {
    '傳承簡介'      => 0,
    '世界心精華寶'  => 1,
    '中心佈告欄'    => 2,
    '活動錦集'      => 3,
    '妙義教授'      => 4,
    '佛學問答'      => 5,
  }

  mount_uploader :thumbnail, ImageUploader

  belongs_to :user

  validates :title, :content, :category, presence: true

  before_validation :set_thumbnail

  private

    def set_thumbnail
      doc = Nokogiri::HTML content
      # https://github.com/carrierwaveuploader/carrierwave/wiki/How-to:-Upload-files-from-a-remote-location-to-JSONB-or-Array-image-column
      if doc.xpath('//img').any?
        self.remote_thumbnail_url = doc.xpath('//img')[0].attr :src
      else
        self.remove_thumbnail!
      end
    end
end
