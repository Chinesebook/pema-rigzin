class User < ApplicationRecord
  has_many :articles

  validates :name, :account, presence: true, uniqueness: true
  has_secure_password
end
