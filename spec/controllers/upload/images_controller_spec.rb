require 'rails_helper'

RSpec.describe Upload::ImagesController, type: :controller do
  render_views

  before do
    @file = fixture_file_upload('images/1523126080668.jpg')
  end

  # it 'can upload a image' do
  #   file = Hash.new
  #   file['image'] = @file
  #   post :create, params: { image: { image: file } }
  #   expect(response).to be_success
  # end

  it 'can upload a image' do
    post :create, params: { image: { image: @file } }
    expect(response).to have_http_status(:success)
  end

  it 'can get url as json' do
    post :create, params: { format: 'json', image: { image: @file } }
    # expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['url']).to include('/uploads/')
  end
end
