require 'rails_helper'

RSpec.describe Article, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"

  let(:article) { Article.new(user_id: 1) }

  it 'must have a title' do
    article.valid?
    expect(article.errors[:title]).not_to be_empty
  end

  it 'must have a content' do
    article.valid?
    expect(article.errors[:content]).not_to be_empty
  end

  it 'must have a category' do
    article.valid?
    expect(article.errors[:category]).not_to be_empty
  end
end
