require 'rails_helper'

RSpec.describe User, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"

  let(:kaban) { User.create(account:  'kaban_san',
                            name:     'kaban',
                            password: '1234asdf' ) }
  let(:subaru) { User.create(account:  'subaru_san',
                             name:     'subaru',
                             password: '1234asdf' ) }

  it 'must set admin false by default' do
    expect(kaban.admin).to be_falsey
  end

  it 'must have a unique name' do
    kaban
    subaru.name = 'kaban'
    subaru.valid?
    expect(subaru.errors[:name]).not_to be_empty
  end

  it 'must have a unique account' do
    kaban
    subaru.account = 'kaban_san'
    subaru.valid?
    expect(subaru.errors[:account]).not_to be_empty
  end
end
