require 'rails_helper'

RSpec.describe Upload::Image, type: :model do
  let(:image) { Upload::Image.new }
  let(:uploader) { ImageUploader.new(image, :image) }

  before do
    File.open('spec/fixtures/images/1523126080668.jpg') { |f| uploader.store!(f) }
  end

  after do
    uploader.remove!
  end

  it 'should be present' do
    expect(uploader).to be_present
  end
end
