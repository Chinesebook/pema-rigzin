FactoryBot.define do
  factory :image_articles, class: Article do
    title      'article test'
    category   1
    user_id    1
    content    '<p><img src=\"http://admin.pema-rigzin.test/uploads/image/40/%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7_2017-08-12_%E4%B8%8A%E5%8D%883.30.11.png\"></p><p><img src=\"https://pbs.twimg.com/media/De0zquYUcAAmy_H.jpg\"></p>'
  end
end
