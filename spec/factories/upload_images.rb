FactoryBot.define do
  factory :upload_image, class: 'Upload::Image' do
    image "MyString"
  end
end
